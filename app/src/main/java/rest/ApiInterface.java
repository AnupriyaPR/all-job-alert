package rest;


import com.app.alljobalerts.model.ServiceModel;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;


/**
 * Created by ubundu on 26/5/16.
 */
public interface ApiInterface {
   /* @GET("/{path}")
    public void getData(@Path(value = "path", encode = false) String end_url, @Query(value = "category") String path, Callback<ServiceModel> response);
*/
   @GET("/{path}")
    public void getData(@Path(value = "path", encode = false) String end_url,Callback<ServiceModel> response);


//    public void getData(@Part("patientId") String part, Callback<ServiceModel> response);

}
