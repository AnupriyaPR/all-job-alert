package com.app.alljobalerts.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


/**
 * Created by ubundu on 26/5/16.
 */
public class ServiceModel extends RealmObject {
    @PrimaryKey
    private long id;

    @SerializedName("result")
    private RealmList<ItemModel> result;

    @SerializedName("response")
    private ResponseModel response;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ResponseModel getResponse() {
        return response;
    }

    public void setResponse(ResponseModel response) {
        this.response = response;
    }

    public RealmList<ItemModel> getResult() {
        return result;
    }

    public void setResult(RealmList<ItemModel> result) {
        this.result = result;
    }
}
