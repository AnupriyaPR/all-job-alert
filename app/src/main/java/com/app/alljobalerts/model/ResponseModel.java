package com.app.alljobalerts.model;


import io.realm.RealmObject;

/**
 * Created by ubundu on 27/5/16.
 */
public class ResponseModel extends RealmObject {
    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
