package com.app.alljobalerts.appUtil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ubundu on 13/6/16.
 */
public class Util {
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

    public static String compareDate(Date date) {
        String details = null;
        try {
            Calendar calendar = Calendar.getInstance();
//            calendar.add(Calendar.DATE, 1);/*Add one day with today for getting exact difference*/
            String todayInString = sdf.format(calendar.getTime());
            Date today = sdf.parse(todayInString);
            long difference = Math.abs(today.getTime() - date.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);

            Log.d("Today:", today + " getDate:" + date + " diff:" + differenceDates);
            //Convert long to String
            String dayDifference = Long.toString(differenceDates);
            if (differenceDates == 0) {
                details = "Today";
            } else if (differenceDates == 1) {
                details = "Yesterday";
            } else if (differenceDates < 7) {
                details = differenceDates + " days ago";
            } else if (differenceDates < 30) {
                details = differenceDates / 7 + " weeks ago";
            } else {
                details = differenceDates / 30 + " months ago";
            }

            Log.e("HERE", "HERE: " + dayDifference);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return details;
    }

    public static boolean haveNetworkConnection(Context context) {
        boolean network = false;
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        network = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return network;
    }
}
