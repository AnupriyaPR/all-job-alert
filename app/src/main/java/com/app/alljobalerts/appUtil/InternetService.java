package com.app.alljobalerts.appUtil;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.app.alljobalerts.activity.ItemList;
import com.app.alljobalerts.model.ItemModel;
import com.app.alljobalerts.model.ServiceModel;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import rest.ApiInterface;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * Created by ubundu on 30/5/16.
 */
public class InternetService {


    //    public static String ROOT_URL = "http://192.168.0.133/resultApp/";
    public static String ROOT_URL = "http://phpstack-8724-47956-124337.cloudwaysapps.com/api/";
    RealmList<ItemModel> arrayList = new RealmList<>();
    Realm realm;

    public void getData(final Activity activity, String endUrl, String category) {
        realm = ItemRealm.with(activity).getRealm();

        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("key", "12345");
            }
        };
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .registerTypeAdapter(new TypeToken<RealmList<ServiceModel>>() {
                }.getType(), new TypeAdapter<RealmList<ServiceModel>>() {

                    @Override
                    public void write(JsonWriter out, RealmList<ServiceModel> value) throws IOException {
                    }

                    @Override
                    public RealmList<ServiceModel> read(JsonReader in) throws IOException {
                        RealmList<ServiceModel> list = new RealmList<ServiceModel>();
                        try {

                            in.beginArray();
                            while (in.hasNext()) {
                                list.add(new ServiceModel());
                            }
                            in.endArray();
                        } catch (java.io.IOException e) {
                            e.printStackTrace();
                        }
                        return list;
                    }
                })
                .create();
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL)
                .setRequestInterceptor(requestInterceptor)
                .setConverter(new GsonConverter(gson))
                .build();

        ApiInterface api = adapter.create(ApiInterface.class);
        api.getData(endUrl, new Callback<ServiceModel>() {
            @Override
            public void success(ServiceModel serviceModel, Response response) {
//                new ItemRealm(activity).clearAll();
                arrayList = new RealmList<ItemModel>();

                if (serviceModel != null) {
                    new ItemRealm(activity).clearAll();
                    serviceModel.setId(new ItemRealm(activity).getRawCount());
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(serviceModel);
                    realm.commitTransaction();
                    arrayList = serviceModel.getResult();

                    ((ItemList) activity).display(arrayList);
                } else {
                    arrayList = new RealmList<ItemModel>();
                }
            }


            @Override
            public void failure(RetrofitError error) {
                Log.d("ResponceError", error + "");
                arrayList = new RealmList<ItemModel>();
                ((ItemList) activity).display(arrayList);
                if (error.toString().contains("Network is unreachable")) {
                    Toast.makeText(activity, "No network", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
