package com.app.alljobalerts.appUtil;

import android.app.Activity;

import com.app.alljobalerts.model.ItemModel;
import com.app.alljobalerts.model.ServiceModel;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by ubundu on 7/6/16.
 */
public class ItemRealm {
    public static ItemRealm instance;
    private static Realm realm;

    public ItemRealm(Activity activity) {

        final RealmConfiguration config = new RealmConfiguration
                .Builder(activity)
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(config);
        realm = Realm.getDefaultInstance();
    }

    public static ItemRealm with(Activity activity) {

        if (instance == null) {
            instance = new ItemRealm(activity);
        }
        return instance;
    }


    public Realm getRealm() {

        return realm;
    }


    public static RealmResults<ItemModel> getDataByCategory(String category) {

        return realm.where(ItemModel.class).equalTo("category", category).findAll();
    }

    public static RealmResults<ItemModel> getAll() {

        return realm.where(ItemModel.class).findAll();
    }

    public long getRawCount() {
        long count;
        realm.beginTransaction();
        count = realm.where(ServiceModel.class).count();
        realm.commitTransaction();
        return count;
    }

    public long getItemRawCount() {
        long count;
        realm.beginTransaction();
        count = realm.where(ItemModel.class).count();
        realm.commitTransaction();
        return count;
    }

    public void clearAll() {

        realm.beginTransaction();
        realm.clear(ServiceModel.class);
        realm.commitTransaction();
        realm.beginTransaction();
        realm.clear(ItemModel.class);
        realm.commitTransaction();
    }
}
