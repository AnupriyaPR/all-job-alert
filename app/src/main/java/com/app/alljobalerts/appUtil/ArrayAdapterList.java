package com.app.alljobalerts.appUtil;

/**
 * Created by ubundu on 30/5/16.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


import com.app.alljobalerts.activity.DescriptionPage;
import com.app.alljobalerts.activity.ItemList;
import com.app.alljobalerts.activity.WebViewLoader;
import com.app.alljobalerts.model.ItemModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.app.alljobalerts.R;

import io.realm.RealmList;

/**
 * Created by ubundu on 21/5/16.
 */
public class ArrayAdapterList extends RecyclerView.Adapter<ArrayAdapterList.MyViewHolder> {
    Activity activity;
    RealmList<ItemModel> array;
    String[] arrayDesc;
    private static LayoutInflater inflater = null;

    public ArrayAdapterList(Activity activity, RealmList<ItemModel> array) {
        this.activity = activity;
        this.array = array;
        inflater = (LayoutInflater) activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtName;
        TextView txtDesc;
        TextView txtDate;
        ImageView imageView;
        String imgUrl = "";

        public MyViewHolder(View v) {
            super(v);
            txtName = (TextView) v.findViewById(R.id.txtNAme);
            txtDesc = (TextView) v.findViewById(R.id.txtDesc);
            imageView = (ImageView) v.findViewById(R.id.img);
            txtDate = (TextView) v.findViewById(R.id.txtDate);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(activity, DescriptionPage.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("Title", array.get(getAdapterPosition()).getName());
            intent.putExtra("Url", array.get(getAdapterPosition()).getUrl());
            intent.putExtra("Image", array.get(getAdapterPosition()).getImage());
            intent.putExtra("Description", array.get(getAdapterPosition()).getDetailedDesc());
            String date = array.get(getAdapterPosition()).getDate();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date1 = sdf.parse(date);
                intent.putExtra("date", Util.compareDate(date1));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            activity.startActivity(intent);
            if (ItemList.showAdd < 1)
                ItemList.showAdd = ItemList.showAdd + 1;
            else
                ItemList.showAdd = 0;
        }
    }

    @Override
    public int getItemCount() {
        return array.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.txtName.setText(array.get(position).getName());
        holder.txtDesc.setText(array.get(position).getDescription());
        String date = array.get(position).getDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date1 = sdf.parse(date);
            holder.txtDate.setText(Util.compareDate(date1));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (!array.get(position).getImage().contains("http"))
            holder.imgUrl = InternetService.ROOT_URL.replace("/api", "") + "uploads/" + array.get(position).getImage();
        Glide.with(activity).load(holder.imgUrl)
                .thumbnail(1.f)
                .crossFade()
                .placeholder(R.drawable.place_holder_large)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imageView);


    }
}
