package com.app.alljobalerts.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.alljobalerts.appUtil.Util;
import com.app.alljobalerts.appUtil.VerticalSpaceItemDecoration;
import com.app.alljobalerts.model.ItemModel;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.app.alljobalerts.appUtil.ArrayAdapterList;
import com.app.alljobalerts.appUtil.InternetService;
import com.app.alljobalerts.appUtil.ItemRealm;
import com.app.alljobalerts.R;

import java.util.ArrayList;
import java.util.Collections;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class ItemList extends AppCompatActivity {
    Activity activity;
    RecyclerView recItem;
    ArrayAdapterList adapterList;
    RealmList<ItemModel> arrayList;
    String[] array = {"Test", "TEst2"};
    String[] arrayDesc = {"sgdfgdfg", "dfgfdgdfgfd"};
    String item;
    TextView txtNoData;
    ProgressBar progressBar;
    Toolbar toolbar;
    private AdView mAdView;
    ProgressDialog progressDialog;
    FloatingActionButton floatingActionButton;
    int clicked = 0;
    Realm realm;
    FloatingActionButton testView;
    public static int showAdd = 1;/*showAdd used for showing add alternatively on each itemClick on Description page*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);
        item = getIntent().getStringExtra("Item");
        activity = this;
        recItem = (RecyclerView) findViewById(R.id.recItem);
        txtNoData = (TextView) findViewById(R.id.txtNoData);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.floating);
        testView = (FloatingActionButton) findViewById(R.id.testView);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        arrayList = new RealmList<>();
        /*Admob*/
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mAdView.loadAd(adRequest);
        progressBar.getIndeterminateDrawable()
                .setColorFilter(0xffcc0000, PorterDuff.Mode.MULTIPLY);


        /*Realm*/
        realm = ItemRealm.with(activity).getRealm();
        arrayList = new RealmList<>();
        adapterList = new ArrayAdapterList(this, arrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recItem.setLayoutManager(mLayoutManager);
        recItem.setItemAnimator(new DefaultItemAnimator());
        recItem.addItemDecoration(new VerticalSpaceItemDecoration(1));
        recItem.setAdapter(adapterList);

        if (Util.haveNetworkConnection(activity))
            new InternetService().getData(this, "fetchAll", "");
        else {
            progressBar.setVisibility(View.GONE);
            onActivityResult(1, 1, null);
        }

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, Categories.class);
                startActivityForResult(intent, 1);
            }

           /* {
                //Creating the instance of PopupMenu


                PopupMenu popup = new PopupMenu(activity, testView);

                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_main, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        progressDialog = new ProgressDialog(activity);
                        progressDialog.setMessage("Loading...");
                        int id = item.getItemId();
                        RealmList<ItemModel> array = new RealmList<>();
                        RealmResults<ItemModel> realmResults = null;

                        switch (id) {

                            case R.id.action_dropdown1:
                                realmResults = new ItemRealm(activity).getDataByCategory("governmentJobs");
                                break;


                            case R.id.action_dropdown2:
                                realmResults = new ItemRealm(activity).getDataByCategory("bankJobs");

                                break;


                            case R.id.action_dropdown3:
                                realmResults = new ItemRealm(activity).getDataByCategory("railwayJobs");

                                break;


                            case R.id.action_dropdown4:
                                realmResults = new ItemRealm(activity).getDataByCategory("teachingJob");
                                array = new RealmList<>();
                                for (int i = 0; i < realmResults.size(); i++) {
                                    array.add(realmResults.get(i));
                                }
                                display(array);
                                break;


                            case R.id.action_dropdown5:
                                realmResults = new ItemRealm(activity).getDataByCategory("itJobs");

                                break;


                            case R.id.action_dropdown6:
                                realmResults = new ItemRealm(activity).getDataByCategory("defenceJobs");

                                break;


                            case R.id.action_dropdown7:
                                realmResults = new ItemRealm(activity).getDataByCategory("engineeringJobs");
                                array = new RealmList<>();
                                for (int i = 0; i < realmResults.size(); i++) {
                                    array.add(realmResults.get(i));
                                }
                                display(array);
                                break;
                            case R.id.action_dropdown8:
                                realmResults = new ItemRealm(activity).getAll();


                                break;


                        }
                        if (realmResults != null) {
                            realmResults.sort("date", RealmResults.SORT_ORDER_DESCENDING);
                            array = new RealmList<>();
                            for (int i = 0; i < realmResults.size(); i++) {
                                array.add(realmResults.get(i));
                            }
                            display(array);
                        }
                        return true;
                    }
                });
                popup.show();
            }*/
        });

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                mAdView.setVisibility(View.VISIBLE);
//                Toast.makeText(getApplicationContext(), "Ad is loaded!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClosed() {
//                Toast.makeText(getApplicationContext(), "Ad is closed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                mAdView.setVisibility(View.GONE);
//                Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
//                Toast.makeText(getApplicationContext(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                mAdView.setVisibility(View.VISIBLE);

//                Toast.makeText(getApplicationContext(), "Ad is opened!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {

            RealmResults<ItemModel> realmResults = null;
            switch (resultCode) {
                case 1:
                    realmResults = new ItemRealm(activity).getAll();
                    break;
                case 2:
                    realmResults = new ItemRealm(activity).getDataByCategory("governmentJobs");
                    break;
                case 3:
                    realmResults = new ItemRealm(activity).getDataByCategory("bankJobs");
                    break;
                case 4:
                    realmResults = new ItemRealm(activity).getDataByCategory("railwayJobs");
                    break;
                case 5:
                    realmResults = new ItemRealm(activity).getDataByCategory("defenceJobs");
                    break;
                case 6:
                    realmResults = new ItemRealm(activity).getDataByCategory("itJobs");
                    break;
            }
            if (realmResults != null) {
                arrayList.clear();
                realmResults.sort("date", RealmResults.SORT_ORDER_DESCENDING);
                arrayList.addAll(realmResults.subList(0, realmResults.size()));
              /*  arrayList = new RealmList<>();
                for (int i = 0; i < realmResults.size(); i++) {
                    arrayList.add(realmResults.get(i));
                }
                Log.d("ARryalist", arrayList.get(0).getName());*/
                display(arrayList);
            }
        }
    }

    public void display(final RealmList<ItemModel> array) {
        if (array.size() > 0) {
            Log.d("ArrAySize", array.size() + " d");
            adapterList = new ArrayAdapterList(this, array);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recItem.setLayoutManager(mLayoutManager);
            recItem.setItemAnimator(new DefaultItemAnimator());
            recItem.setAdapter(adapterList);
        } else {
            txtNoData.setVisibility(View.VISIBLE);
            txtNoData.setText("No Jobs");
        }
        progressBar.setVisibility(View.GONE);
        Log.d("ArraySize", array.size() + " d");
       /* new Handler().post(new Runnable() {
            @Override
            public void run() {*/

     /*   progressBar.setVisibility(View.GONE);
        if (progressDialog != null) {
            progressDialog.hide();
        }
        arrayList.clear();
        arrayList = array;
        if (array != null) {
                  *//*  for (int i = 0; i < array.size(); i++) {
                        ItemModel model = array.get(i);
                        realm.beginTransaction();
                        if (!model.getImage().contains("http"))
                            model.setImage(InternetService.ROOT_URL.replace("/api", "") + "uploads/" + model.getImage());


//                realm.copyToRealmOrUpdate(model);
                        realm.commitTransaction();

                        arrayList.add(model);
                        adapterList.notifyDataSetChanged();
                    }*//*
//            arrayList = new ArrayList<ItemModel>(array);
            if (array.size() > 0) {
                txtNoData.setVisibility(View.GONE);
            } else {
                txtNoData.setVisibility(View.VISIBLE);
                txtNoData.setText("No Jobs");
            }
        } else {
            txtNoData.setVisibility(View.VISIBLE);
            txtNoData.setText("No Jobs");
        }
        adapterList.notifyDataSetChanged();*/
            /*}
        });
*/

    }

   /* @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }
*//*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }*//*


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }*/

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }
}
