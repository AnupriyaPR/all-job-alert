package com.app.alljobalerts.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout;

import com.app.alljobalerts.R;

public class Categories extends Activity {
    LinearLayout lytAllJob;
    LinearLayout lytGov;
    LinearLayout lytBank;
    LinearLayout lytRailway;
    LinearLayout lytDefence;
    LinearLayout lytIt;
    LinearLayout lytBg;
    View.OnClickListener onClickListener;

    String[] array = {"LatestExamResults", "StateWiseResults", "AcademicResults", "EntranceResults", "Apps"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categories);
        lytAllJob = (LinearLayout) findViewById(R.id.lytAllJobs);
        lytBank = (LinearLayout) findViewById(R.id.lytBank);
        lytDefence = (LinearLayout) findViewById(R.id.lytDefence);
        lytGov = (LinearLayout) findViewById(R.id.lytGovJob);
        lytIt = (LinearLayout) findViewById(R.id.lytIt);
        lytRailway = (LinearLayout) findViewById(R.id.lytRailway);
        lytBg = (LinearLayout) findViewById(R.id.lytBg);
        onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String item = "";
                switch (v.getId()) {
                    case R.id.lytAllJobs:
                        setResult(1);
                        finish();
                        break;
                    case R.id.lytGovJob:
                        setResult(2);
                        finish();
                        break;
                    case R.id.lytBank:
                        setResult(3);
                        finish();
                        break;
                    case R.id.lytRailway:
                        setResult(4);
                        finish();
                        break;
                    case R.id.lytDefence:
                        setResult(5);
                        finish();
                        break;
                    case R.id.lytIt:
                        setResult(6);
                        finish();
                        break;
                    case R.id.lytBg:
                        finish();
                        break;
                }
            /*    Log.d("Item", item);
                Intent intent = new Intent(Categories.this, ItemList.class);
                intent.putExtra("Item", item);
                startActivity(intent);*/
            }
        };
        lytIt.setOnClickListener(onClickListener);
        lytRailway.setOnClickListener(onClickListener);
        lytGov.setOnClickListener(onClickListener);
        lytDefence.setOnClickListener(onClickListener);
        lytBank.setOnClickListener(onClickListener);
        lytAllJob.setOnClickListener(onClickListener);
        lytBg.setOnClickListener(onClickListener);
    }
}
