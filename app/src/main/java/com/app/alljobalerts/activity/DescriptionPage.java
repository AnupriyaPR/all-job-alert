package com.app.alljobalerts.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.alljobalerts.R;
import com.app.alljobalerts.appUtil.InternetService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class DescriptionPage extends AppCompatActivity {
    TextView txtDescription;
    ImageView imgLogo;
    String title;
    ImageView imgBack;
    Button btnViewJob;
    String description;
    String url;
    String image;
    String date;
    Activity activity;
    TextView txtTitle;
    TextView txtDate;
    String imgUrl;
    InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description_page);
        activity = this;
        txtDescription = (TextView) findViewById(R.id.txtDescription);
        imgLogo = (ImageView) findViewById(R.id.imgLogo);
        btnViewJob = (Button) findViewById(R.id.btnViewJob);
        txtTitle = (TextView) findViewById(R.id.txtJobTitle);
        imgBack = (ImageView) findViewById(R.id.imgback);
        txtDate = (TextView) findViewById(R.id.txtDate);

        title = getIntent().getStringExtra("Title");
        description = getIntent().getStringExtra("Description");
        url = getIntent().getStringExtra("Url");
        image = getIntent().getStringExtra("Image");
        date = getIntent().getStringExtra("date");

        txtDate.setText(date);
        txtTitle.setText(title);
        txtDescription.setText(description);
        if (!image.contains("http"))
            imgUrl = InternetService.ROOT_URL.replace("/api", "") + "uploads/" + image;
        Glide.with(activity).load(imgUrl)
                .thumbnail(1.f)
                .crossFade()
                .placeholder(R.drawable.place_holder_large)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgLogo);

        btnViewJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, WebViewLoader.class);
                intent.putExtra("Title", title);
                intent.putExtra("Url", url);
                activity.startActivity(intent);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgBack.startAnimation(new AlphaAnimation(1.0f, 00f));
                onBackPressed();
            }
        });

        if (ItemList.showAdd == 0) {
          /*AdMob*/
            mInterstitialAd = new InterstitialAd(this);

            // set the ad unit ID
            mInterstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));

            AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .build();

            // Load ads into Interstitial Ads
            mInterstitialAd.loadAd(adRequest);

            mInterstitialAd.setAdListener(new AdListener() {
                public void onAdLoaded() {
                    showInterstitial();
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
//                Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
                }

            });
        }
    }

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }
}
