package com.app.alljobalerts.app;

import android.app.Application;
import android.content.Context;

import com.app.alljobalerts.R;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import io.realm.Realm;
import io.realm.RealmConfiguration;

@ReportsCrashes(
        mailTo = "info@wiinnova.com",
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_toast_text)
public class MyApplication extends Application {

    @Override
    public void onCreate() {

        super.onCreate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        ACRA.init(this);
    }

}
